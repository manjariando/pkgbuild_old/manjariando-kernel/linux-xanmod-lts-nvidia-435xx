# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

_linuxprefix=linux-xanmod-lts
_extramodules=extramodules-5.10-xanmod
# don't edit here
pkgver=435.21_5.10.82.xanmod1_1

_nver=435
# edit here for new version
_sver=21
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-${_nver}xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
makedepends=("$_linuxprefix" "$_linuxprefix-headers" "nvidia-${_nver}xx-utils=${_pkgver}")
groups=("$_linuxprefix-extramodules")
provides=("${_pkgname}=${_pkgver}" "nvidia-${_nver}xx-modules=${_pkgver}")
conflicts=("$_linuxprefix-nvidia" "$_linuxprefix-nvidia-340xx" "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-418xx"
           "$_linuxprefix-nvidia-430xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx" "$_linuxprefix-nvidia-455xx"
           "$_linuxprefix-nvidia-460xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="http://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run"
        "license.patch" "kernel-5.4.patch" "kernel-5.5.patch" "kernel-5.6.patch"
        "kernel-5.7.patch" "kernel-5.8.patch" "kernel-5.9.patch" "kernel-5.10.patch")
sha256sums=('caee54f0ee5f6171a61b25670d309d1abe16d59fc7bec0577794b1a52c09244a'
            'd6a6289ad4aa675033ec0a2ef4f808605fdcfbdbb319b78fff3f84cd7e42d988'
            '542050033219ce17e60b12e4ca6a79d21643411901e9a4517a378b4d4ff249c2'
            '088146db7e53176c727177aea6d2d7273513a6198cbafd00a3e59c32cc4e34e6'
            '79366a2fc810c0b63dcb399f73405029e82bb56b11d604d5d22a9574d4d3b303'
            'c1db505ca4e4dded42b3be846bf8a136b0295852bf977d22f00487740a50f1a0'
            '39c665f190cbd85f0d0b395a933d8b646f0d7c49a568ac32018f1a5d339cb419'
            'cb927234e33b7dfb37245074d642e609e54daf5aebd24f6e69d59f30c7e68fa4'
            '3e52ac2858e2cf480bda38b3d821050fd670ba8a3b4ad7bdeec959e40d466728')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    # patches here
    # original patches https://www.if-not-true-then-false.com/2020/inttf-nvidia-patcher
    # Fix compile problem with license
    msg2 "PATCH: license"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/license.patch

    # Fix compile problem with 5.4
    msg2 "PATCH: kernel-5.4"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.4.patch

    # Fix compile problem with 5.5
    msg2 "PATCH: kernel-5.5"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.5.patch

    # Fix compile problem with 5.6
    msg2 "PATCH: kernel-5.6"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.6.patch

    # Fix compile problem with 5.7
    msg2 "PATCH: kernel-5.7"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.7.patch

    # Fix compile problem with 5.8
    msg2 "PATCH: kernel-5.8"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.8.patch

    # Fix compile problem with 5.9
    msg2 "PATCH: kernel-5.9"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.9.patch

    # Fix compile problem with 5.10
    msg2 "PATCH: kernel-5.10"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.10.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")
    replaces=("linux510-xanmod-nvidia-${_nver}xx")

    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-modeset.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-modeset.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-drm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-drm.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-uvm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
